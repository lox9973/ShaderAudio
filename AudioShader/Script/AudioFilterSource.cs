using UnityEngine;
#if UDON
using UdonSharp;
#endif
namespace ShaderAudio {
[RequireComponent(typeof(AudioSource))]
public class AudioFilterSource:
#if UDON
	UdonSharpBehaviour
#else
	MonoBehaviour
#endif
{
	public int sampleRate = 48000;
	[System.NonSerialized] public Color[] buffer;
	[System.NonSerialized] public int offset = 0;
	void OnAudioFilterRead(float[] data, int channels) {
		// save to reduce data race
		var buf = buffer;
		var off = offset;
		if(buffer == null)
			return;
		// save for performance
		var nbuf = buf.Length;
		var ndata = data.Length;
		for(int i = 0; i < ndata; off ++) {
			var color = buf[off % nbuf];
			data[i] = color.r; i++;
			data[i] = color.g; i++;
		}
		offset = off;
	}
	// don't implement Update() here! it'll crash the Udon stack
	private float[] onAudioFilterReadData;
	private int onAudioFilterReadChannels;
	public void _onAudioFilterRead() { // expose OnAudioFilterRead to UdonSharp
		OnAudioFilterRead(onAudioFilterReadData, onAudioFilterReadChannels);
	}
}
}