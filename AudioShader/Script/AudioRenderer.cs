using UnityEngine;
#if UDON
using UdonSharp;
#endif
namespace ShaderAudio {
[RequireComponent(typeof(Camera), typeof(MeshRenderer))]
public class AudioRenderer:
#if UDON
	UdonSharpBehaviour
#else
	MonoBehaviour
#endif
{
	public AudioFilterSource audioFilterSource;
	public Texture2D texture;

	private Renderer renderer;
	private Rect rect;
	private Color[] buffer;
	private MaterialPropertyBlock matPropBlock;
	void OnEnable() {
		var width = texture.width;
		var height = texture.height;
		rect = new Rect(0, 0, width, height);
		buffer = new Color[width * height];
		audioFilterSource.buffer = buffer;
		audioFilterSource.offset = 0;

		renderer = GetComponent<MeshRenderer>();
		matPropBlock = new MaterialPropertyBlock();
		matPropBlock.SetInt("_SampleRate", audioFilterSource.sampleRate);
		matPropBlock.SetInt("_Offset", audioFilterSource.offset);
		renderer.SetPropertyBlock(matPropBlock);
	}
	void Start() {
		var rt = GetComponent<Camera>().targetTexture;
		rt.width = texture.width;
		rt.height = texture.height;
	}
	void OnDisable() {
		System.Array.Clear(buffer, 0, buffer.Length);
	}
	void OnPreRender() {
		matPropBlock.SetInt("_Offset", audioFilterSource.offset);
		renderer.SetPropertyBlock(matPropBlock);
	}
	void OnPostRender() {
		texture.ReadPixels(rect, 0, 0, false);
		System.Array.Copy(texture.GetPixels(), buffer, buffer.Length);
	}
}
}