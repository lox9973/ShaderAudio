#include <UnityCG.cginc>

uint _SampleRate;
uint _Offset;
static float2 _BufferSize = _ScreenParams.xy; // use render target size

struct FragInput {
	float2 tex : TEXCOORD0;
	float4 pos : SV_Position;
	UNITY_VERTEX_OUTPUT_STEREO
};
void vert(appdata_base i, out FragInput o) {
	UNITY_SETUP_INSTANCE_ID(i);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
	o.tex = i.texcoord;
	o.pos = float4(i.texcoord.xy*2-1, UNITY_NEAR_CLIP_VALUE, 1);
	o.pos.y *= _ProjectionParams.x;
	if(unity_OrthoParams.w != 1)
		o.pos = 0;
}
float2 mainSound(int samp, float time);
float2 frag(FragInput i) : SV_Target {
	uint bufferLen = _BufferSize.x * _BufferSize.y;
	uint index = dot(floor(i.tex.xy * _BufferSize.xy), float2(1, _BufferSize.x));
	index += (_Offset/bufferLen + (_Offset%bufferLen > index)) * bufferLen;
	return mainSound(index, float(index / _SampleRate) + float(index % _SampleRate) / _SampleRate);
}